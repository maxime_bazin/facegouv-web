import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {Resource} from '../../models/resource';
import {ResourcesService} from '../../services/resources.service';

@Component({
  selector: 'app-resources-self',
  templateUrl: './resources-self.component.html',
  styleUrls: ['./resources-self.component.scss']
})
export class ResourcesSelfComponent implements OnInit {
  resources: Resource[];

  constructor(private resourceService: ResourcesService, private authService: AuthService) {
  }

  async ngOnInit(): Promise<void> {
    this.subscribeToResources();
    await this.resourceService.getAllResourcesByUser(this.authService.user.id);
  }

  subscribeToResources(): void {
    this.resourceService.resourcesByUserSubject.subscribe(
      (resources: Array<Resource>) => {
        this.resources = resources;
      },
      (error) => {
        console.error(error);
      }
    );
  }
}
