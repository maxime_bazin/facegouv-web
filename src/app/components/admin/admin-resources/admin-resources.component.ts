import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {Resource} from '../../../models/resource';
import {MatDialog} from '@angular/material/dialog';
import {ResourcesService} from '../../../services/resources.service';
import {DialogAdminResourceComponent} from '../../dialogs/dialog-admin-resource/dialog-admin-resource.component';
import {User} from '../../../models/user';
import {AuthService} from '../../../services/auth.service';

@Component({
  selector: 'app-admin-resources',
  templateUrl: './admin-resources.component.html',
  styleUrls: ['./admin-resources.component.scss']
})
export class AdminResourcesComponent implements OnInit, AfterViewInit {
  ELEMENT_DATA: Resource[];
  displayedColumns: string[] = ['id', 'category', 'state', 'title', 'user', 'createdAt'];
  // @ts-ignore
  dataSource;
  userState;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private service: ResourcesService, private dialog: MatDialog, private authService: AuthService) {
    this.userState = this.authService.user ? this.authService.user.userStateId : 2;
    this.dataSource = new MatTableDataSource<Resource>(this.ELEMENT_DATA);
  }

  ngOnInit(): void {
    this.getAllResources();
  }

  public getAllResources(): void {
    const data = this.service.getAllResources();
    data.subscribe(resource => {
      this.dataSource.data = resource as Resource[];
    });
  }

  showDialog(data): void {
    this.dialog.open(DialogAdminResourceComponent, {data});
  }

  applyFilter(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  getColor(value): any {
    switch (value) {
      case 1:
        return '#FF6F4C';
      case 2:
        return '#169B62';
      case 3:
        return '#FF8D7E';
      default:
        return '#FFEB68';
    }
  }

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }
}
