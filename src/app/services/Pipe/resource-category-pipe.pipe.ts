import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'resourceCategoryPipe'
})
export class ResourceCategoryPipePipe implements PipeTransform {

  transform(value): void {
    switch (value) {
      case 1:
        value = 'Publication';
        break;
      case 2:
        value = 'Activité';
        break;
      default:
        value = 'Événement';
    }
    return value;
  }

}
