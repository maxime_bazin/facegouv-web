import {Injectable} from '@angular/core';
import io from 'socket.io-client';
import {ToastrService} from 'ngx-toastr';
import {AppEventManagerService} from './app-event-manager.service';
import {UserService} from './user.service';
import {Global} from '../global';

@Injectable({
  providedIn: 'root'
})
export class SocketService {

  socket: any;

  constructor(private global: Global, private toastr: ToastrService, private appEventManagerService: AppEventManagerService) {
  }

  turnOnSocket(userId): void {
    this.socket = io(this.global.apiUrl, {path: '/socket'});
    // Login
    this.socket.on('askForUserId', () => {
      this.onAskUserId(userId);
    });
    // Errors
    this.socket.on('error', ({message}) => {
      this.toastr.error(message);
    });
    this.socket.on('information', ({message}) => {
      this.toastr.info(message);
    });
    // Conversations
    this.socket.on('conversationCreated', ({message}) => {
      this.toastr.success(message);
      this.appEventManagerService.broadcast('conversationChanged');
    });
    this.socket.on('conversationRenamed', ({message}) => {
      this.toastr.success(message);
      this.appEventManagerService.broadcast('conversationChanged');
    });
    this.socket.on('messageAdded', ({message}) => {
      this.toastr.success(message);
      this.appEventManagerService.broadcast('conversationChanged');
    });
    this.socket.on('messageRemoved', ({message}) => {
      this.toastr.success(message);
      this.appEventManagerService.broadcast('conversationChanged');
    });
    this.socket.on('userAddedToConv', ({message}) => {
      this.toastr.success(message);
      this.appEventManagerService.broadcast('conversationChanged');
    });
    this.socket.on('messagesReaded', ({message}) => {
      this.toastr.success(message);
      this.appEventManagerService.broadcast('conversationChanged');
    });
    this.socket.on('userLeft', ({message}) => {
      this.toastr.success(message);
      this.appEventManagerService.broadcast('conversationChanged');
    });
    this.socket.on('conversationDeleted', ({message}) => {
      this.toastr.success(message);
      this.appEventManagerService.broadcast('conversationChanged');
    });
    this.socket.on('kickedFromConversation', ({message}) => {
      this.toastr.success(message);
      this.appEventManagerService.broadcast('conversationChanged');
    });
    this.socket.on('userKickedFromConv', ({message}) => {
      this.toastr.success(message);
      this.appEventManagerService.broadcast('conversationChanged');
    });
    // Relationships
    this.socket.on('friendAdded', ({message}) => {
      this.toastr.success(message);
      this.appEventManagerService.broadcast('friendChanged');
    });
    this.socket.on('friendRemoved', ({message}) => {
      this.toastr.success(message);
      this.appEventManagerService.broadcast('friendChanged');
    });
    this.socket.on('friendshipConfirmed', ({message}) => {
      this.toastr.success(message);
      this.appEventManagerService.broadcast('friendChanged');
    });
    // Resources
    this.socket.on('resourceShared', ({message}) => {
      this.toastr.success(message);
      this.appEventManagerService.broadcast('resourceShared');
    });
  }

  // Ask userId to match it with socketId
  onAskUserId(userId): void {
    this.socket.emit('userIdReceived', userId);
  }

  //------------------------------------------------------------------------------------------//
  // CONVERSATIONS                                                                            //
  //------------------------------------------------------------------------------------------//

  // Create a conversation
  createConv(adminId: number, name: string, usersId: Array<number>): void {
    this.socket.emit('createConv', {adminId, name, usersId});
  }

  // Rename a conversation
  renameConv(userId: number, name: string, conversationId: number): void {
    this.socket.emit('renameConv', {userId, name, conversationId});
  }

  // Delete a conversation
  deleteConv(userId: number, conversationId: number): void {
    this.socket.emit('deleteConv', {userId, conversationId});
  }

  // Post a message in conversation
  addMessageToConv(userId: number, conversationId: number, message: string): void {
    this.socket.emit('addMessage', {userId, conversationId, message});
  }

  // Remove a message from conversation
  removeMessageFromConv(userId: number, messageId: number): void {
    this.socket.emit('removeMessage', {userId, messageId});
  }

  // Add a user to the conversation
  addUserToConv(userId: number, newUserId: number, conversationId: number): void {
    this.socket.emit('addUserToConv', {userId, newUserId, conversationId});
  }

  // Leave the conversation
  leaveConv(userId: number, conversationId: number): void {
    this.socket.emit('leaveConv', {userId, conversationId});
  }

  // Set all conversation messages as readed
  readMessages(userId: number, conversationId: number): void {
    this.socket.emit('readMessages', {userId, conversationId});
  }

  // Kick a user from the conversation
  kickUserFromConv(userId: number, userToKick: number, conversationId: number): void {
    this.socket.emit('kickUserFromConv', {userId, userToKick, conversationId});
  }

  //------------------------------------------------------------------------------------------//
  // RELATIONSHIPS                                                                            //
  //------------------------------------------------------------------------------------------//
  // Add a user to friends
  addUserToFriends(senderId: number, receiverId: number): void {
    this.socket.emit('addFriend', {senderId, receiverId});
  }

  // Remove a user from friends
  removeUserFromFriends(userId: number, relationId: number): void {
    this.socket.emit('removeFriend', {userId, relationId});
  }

  // Confirm friendship
  confirmFriendship(userId: number, relationId: number): void {
    this.socket.emit('confirmFriend', {userId, relationId});
  }

  //------------------------------------------------------------------------------------------//
  // RESOURCES                                                                                //
  //------------------------------------------------------------------------------------------//
  shareResource(senderId: number, receiversId: number[], resourceId: number): void {
    this.socket.emit('shareResource', {senderId, receiversId, resourceId});
  }

  //------------------------------------------------------------------------------------------//
  // DISCONNECTION                                                                            //
  //------------------------------------------------------------------------------------------//
  // Turn off the socket at user disconnection
  async turnOffSocket(): Promise<void> {
    await this.socket.close();
  }
}
