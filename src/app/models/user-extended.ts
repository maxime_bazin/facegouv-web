import {User} from './user';
import {Resource} from './resource';

export class UserExtended extends User {
  resourcesFavorites: Resource[];
  resourcesExploited: Resource[];
  resourcesSaved: Resource[];
  isConnected: boolean;
}
