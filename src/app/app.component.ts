import {Component, OnInit, Output} from '@angular/core';
import {AppEventManagerService} from './services/app-event-manager.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'Facegouv';

  constructor() {
  }

  ngOnInit(): void {
  }
}
