import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../../services/auth.service';


@Component({
  selector: 'app-forgotten-password',
  templateUrl: './forgotten-password.component.html',
  styleUrls: ['./forgotten-password.component.scss']
})
export class ForgottenPasswordComponent implements OnInit {

  email: string;

  constructor(private authService: AuthService) {
  }

  ngOnInit(): void {
  }

  forgottenPassword(): void {
    this.authService.forgottenPassword(this.email);
  }

}
