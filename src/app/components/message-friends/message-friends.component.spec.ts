import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MessageFriendsComponent } from './message-friends.component';

describe('MessageFriendsComponent', () => {
  let component: MessageFriendsComponent;
  let fixture: ComponentFixture<MessageFriendsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MessageFriendsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MessageFriendsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
