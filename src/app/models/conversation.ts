import {User} from './user';
import {MessageConversation} from './messageConversation';

export class Conversation {
  id: number;
  AdminId: number;
  Admin: User;
  name: string;
  UsersIds: number[];
  messages: MessageConversation[];

  constructor(adminId: number) {
    this.AdminId = adminId;
  }
}
