import {Component, ViewChild, OnInit, AfterViewInit} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {UserService} from '../../../services/user.service';
import {User} from '../../../models/user';
import {MatDialog} from '@angular/material/dialog';
import {DialogAdminUserComponent} from '../../dialogs/dialog-admin-user/dialog-admin-user.component';
import {AuthService} from '../../../services/auth.service';
import {Favorite} from '../../../models/favorite';
import {Resource} from "../../../models/resource";

@Component({
  selector: 'app-admin-users',
  templateUrl: './admin-users.component.html',
  styleUrls: ['./admin-users.component.scss']
})
export class AdminUsersComponent implements OnInit, AfterViewInit {
  ELEMENT_DATA: User[];
  displayedColumns: string[] = ['id', 'firstName', 'lastName', 'email', 'age', 'role', 'isEnabled', 'createdAt'];

  dataSource;
  userState;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private userService: UserService, private dialog: MatDialog, private authService: AuthService) {
    this.userState = this.authService.user.userStateId;
    this.dataSource = new MatTableDataSource<User>(this.ELEMENT_DATA);
  }

  ngOnInit(): void {
    this.subscribeToUsers();
    if (this.authService.user.userStateId === 6) {
      this.getAllUsers();
    } else if (this.authService.user.userStateId === 5) {
      this.getAllUsersExceptAdmin();
    } else {
      console.log('pas les droits');
    }
  }

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  async getAllUsers(): Promise<void> {
    await this.userService.getAllUsers();
  }

  async getAllUsersExceptAdmin(): Promise<void> {
    await this.userService.getAllUsersExceptAdmin();
  }

  applyFilter(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  showDialog(data): void {
    this.dialog.open(DialogAdminUserComponent, {data});
  }

  subscribeToUsers(): void {
    this.userService.usersSubject.subscribe(
      (users: Array<User>) => {
        this.dataSource.data = users as User[];
      },
      (error) => {
        console.error(error);
      }
    );
  }
}
