import {Injectable} from '@angular/core';
import {AuthService} from './auth.service';
import {Favorite} from '../models/favorite';
import {Global} from '../global';
import {HttpClient} from '@angular/common/http';
import {Subject} from 'rxjs';
import {ToastrService} from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class FavoriteService {
  favoritesSubject = new Subject();

  constructor(private global: Global, private http: HttpClient, private toastr: ToastrService, private authService: AuthService) {
  }

  async addToFavorite(userId: number, resourceId: number): Promise<void> {
    const body = {
      userId, resourceId
    };
    await this.http.post(`${this.global.apiUrl}favorite`, body).subscribe((data) => {
        this.refreshList();
        this.toastr.success('Favoris ajouté');
      },
      (error => {
        this.toastr.error(error.error.erreur);
      })
    );
  }

  async removeFavorite(id: number): Promise<void> {
    try {
      await this.http.delete(`${this.global.apiUrl}favorite/${id}`).subscribe((data) => {
        this.refreshList();
        this.toastr.success('Favoris supprimé');
      });
    } catch (e) {
      this.toastr.error(e.error.erreur);
    }
  }

  async getAllFavorites(userId: number): Promise<void> {
    const data = await this.http.get(`${this.global.apiUrl}favorites/${userId}`).toPromise();
    this.authService.user.favorites = data as Array<Favorite>;
    this.favoritesSubject.next(data as Array<Favorite>);
  }

  refreshList(): void {
    this.getAllFavorites(this.authService.user.id);
  }
}
