import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../../services/auth.service';

@Component({
  selector: 'app-edit-password',
  templateUrl: './edit-password.component.html',
  styleUrls: ['./edit-password.component.scss']
})
export class EditPasswordComponent implements OnInit {
  password: string;
  newPassword: string;
  confirmPassword: string;

  constructor(private authService: AuthService) {
  }

  ngOnInit(): void {
  }

  editPassword(): void {
    this.authService.editPassword(this.password, this.newPassword, this.confirmPassword);
  }

}
