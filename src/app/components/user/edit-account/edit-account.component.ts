import {Component, OnInit} from '@angular/core';
import {User} from '../../../models/user';
import {AuthService} from '../../../services/auth.service';
import {Global} from '../../../global';
import {UserService} from '../../../services/user.service';
import {AppEventManagerService} from '../../../services/app-event-manager.service';

@Component({
  selector: 'app-edit-account',
  templateUrl: './edit-account.component.html',
  styleUrls: ['./edit-account.component.scss']
})
export class EditAccountComponent implements OnInit {
  user: User;
  image: any;

  constructor(private appEventManagerService: AppEventManagerService, private authService: AuthService, public global: Global, private userService: UserService) {
    this.user = this.authService.user;
  }

  ngOnInit(): void {
    this.subscribeToUser();
  }

  async editAccount(): Promise<void> {
    await this.authService.editAccount(this.user, this.image);
    this.appEventManagerService.broadcast('userUpdated');
    await this.userService.getUserById(this.user.id);
  }

  uploadFile(event): void {
    this.image = event.target.files[0];
  }

  subscribeToUser(): void {
    this.userService.userSubject.subscribe(
      (user: User) => {
        this.user = user;
      },
      error => {
        console.error(error);
      }
    );
  }

}
