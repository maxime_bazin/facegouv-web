import {Component, OnInit} from '@angular/core';
import {User} from '../../models/user';
import {UserService} from '../../services/user.service';
import {AuthService} from '../../services/auth.service';
import {Relation} from '../../models/relation';
import {AppEventManagerService} from '../../services/app-event-manager.service';

@Component({
  selector: 'app-friends',
  templateUrl: './friends.component.html',
  styleUrls: ['./friends.component.scss']
})
export class FriendsComponent implements OnInit {
  relations: Relation[];

  get friendsConfirmed(): Relation[] {
    return this.relations?.filter(f => f.isConfirmed);
  }

  get friendsNotConfirmed(): Relation[] {
    return this.relations?.filter(f => !f.isConfirmed);
  }

  constructor(private userService: UserService, private authService: AuthService, private appEventManagerService: AppEventManagerService) {
  }

  async ngOnInit(): Promise<void> {
    this.subscribeToRelations();
    await this.getAllFriends();
    this.appEventManagerService.subscribe('friendChanged', async () => this.getAllFriends());
  }

  async getAllFriends(): Promise<void> {
    await this.userService.getAllFriends(this.authService.user.id);
  }

  subscribeToRelations(): void {
    this.userService.relationsSubject.subscribe(
      (relations: Relation[]) => {
        this.relations = relations;
      },
      error => {
        console.error(error);
      }
    );
  }

  getOtherUserInFriend(friend: Relation): User {
    return this.userService.getOtherUserInFriend(friend, this.authService.user.id);
  }
}
