import {Component, OnInit} from '@angular/core';
import {
  faFile,
  faHome,
  faUsers,
  faAngleDoubleLeft,
  faAngleDoubleRight,
  faQuestion,
  faStar,
  faShareAlt,
  faHeart, faBalanceScaleRight
} from '@fortawesome/free-solid-svg-icons';
import {AuthService} from '../../services/auth.service';
import {UserStates} from '../../models/enums/user-states.enum';
import {AppEventManagerService} from '../../services/app-event-manager.service';
import {ResourcesService} from '../../services/resources.service';
import {Subscription} from 'rxjs';
import {User} from '../../models/user';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  //#region icons;
  faHome = faHome;
  faFile = faFile;
  faUsers = faUsers;
  faAngleDoubleLeft = faAngleDoubleLeft;
  faAngleDoubleRight = faAngleDoubleRight;
  faQuestion = faQuestion;
  faBalanceScaleRight = faBalanceScaleRight;
  faStar = faStar;
  faShareAlt = faShareAlt;
  faHeart = faHeart;

  //#endregion;

  isDeployed: boolean;
  user: User;
  links: typeof NavbarLinks = NavbarLinks;

  numberResourcesNotValided;

  constructor(private authService: AuthService, private eventManager: AppEventManagerService,
              private resourcesService: ResourcesService) {
    this.user = this.authService.user;
  }

  async ngOnInit(): Promise<void> {
    await this.subscribeToUser();
    await this.subscribeToNumberResourcesNotValided();
    this.eventManager.subscribe('toggleNavbarEvent', () => {
      this.deployNavbar();
    });
    await this.authService.getUserConnected();
  }

  async subscribeToUser(): Promise<void> {
    this.authService.userSubject.subscribe(
      async (user: User) => {
        this.user = user as User;
        if (this.user?.userStateId >= 4) {
          await this.resourcesService.getAllResourcesNotValided();
        }
      },
      (error) => {
        console.error(error);
      }
    );
  }

  async subscribeToNumberResourcesNotValided(): Promise<void> {
    await this.resourcesService.numberResourcesNotValidedSubject.subscribe(
      (nb: number) => {
        this.numberResourcesNotValided = nb;
      },
      (error) => {
        console.error(error);
      }
    );
  }

  deployNavbar(): void {
    this.isDeployed = !this.isDeployed;
  }
}

enum NavbarLinks {
  home = 'Accueil',
  about = 'À propos',
  general_conditions = 'Conditions générales',
  myResources = 'Mes ressources',
  favorites = 'Favorites',
  saved = 'Sauvegardées',
  shared = 'Partagées',
  users = 'Utilisateurs',
  resources = 'Ressources'
}
