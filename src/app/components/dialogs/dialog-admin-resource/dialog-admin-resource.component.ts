import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {ResourcesService} from '../../../services/resources.service';

@Component({
  selector: 'app-dialog-admin-resource',
  templateUrl: './dialog-admin-resource.component.html',
  styleUrls: ['./dialog-admin-resource.component.scss']
})
export class DialogAdminResourceComponent implements OnInit {

  state;

  constructor(@Inject(MAT_DIALOG_DATA) public data, private service: ResourcesService) {}

  ngOnInit(): void {
  }

  editResourceFromAdmin(id): void {
    this.service.editResourceStateId(id, this.state);
  }

}
