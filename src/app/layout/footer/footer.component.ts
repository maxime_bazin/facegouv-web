import {Component, Input, OnInit} from '@angular/core';
import {AppEventManagerService} from '../../services/app-event-manager.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  conversationsId: number[];

  constructor(private appEventManagerService: AppEventManagerService) {
  }

  ngOnInit(): void {
    this.updateConversations();
    this.appEventManagerService.subscribe('addConversation', () => this.updateConversations());
  }

  updateConversations(): void {
    this.conversationsId = JSON.parse(localStorage.getItem('conversationsId'));
  }
}
