import {Component, OnInit} from '@angular/core';
import {User} from '../../models/user';
import {UserService} from '../../services/user.service';
import {Resource} from '../../models/resource';
import {AuthService} from '../../services/auth.service';
import {AppEventManagerService} from '../../services/app-event-manager.service';
import {Relation} from '../../models/relation';

@Component({
  selector: 'app-add-friend',
  templateUrl: './add-friend.component.html',
  styleUrls: ['./add-friend.component.scss']
})
export class AddFriendComponent implements OnInit {
  users: User[];
  friends: Relation[];

  constructor(private userService: UserService, private authService: AuthService, private appEventManagerService: AppEventManagerService) {
    this.appEventManagerService.subscribe('friendModified', async () => {
      setTimeout(async () => {
        await this.userService.getAllFriends(this.authService.user.id);
      }, 250);
    });
  }

  async ngOnInit(): Promise<void> {
    this.subscribeToUsers();
    this.subscribeToFriends();
    await this.userService.getAllUsers();
    await this.userService.getAllFriends(this.authService.user.id);
  }

  subscribeToUsers(): void {
    this.userService.usersSubject.subscribe(
      users => {
        const result = users as User[];
        this.users = result.filter(value => value.id !== this.authService.user.id);
      },
      error => {
        console.error(error);
      }
    );
  }

  subscribeToFriends(): void {
    this.userService.relationsSubject.subscribe(
      friends => {
        this.friends = friends as Relation[];
      },
      error => {
        console.error(error);
      }
    );
  }

  getFriend(userId: number): Relation {
    if (this.friends && this.authService.user) {
      return this.userService.getFriend(this.friends, this.authService.user.id, userId);
    }
  }
}
