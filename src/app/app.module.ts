import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {HeaderComponent} from './layout/header/header.component';
import {NavbarComponent} from './layout/navbar/navbar.component';
import {RouterModule, Routes} from '@angular/router';
import {AboutComponent} from './components/about/about.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {ResourcesComponent} from './components/resources/resources.component';
import {CreateAccountComponent} from './components/user/create-account/create-account.component';
import {LoginAccountComponent} from './components/user/login-account/login-account.component';
import {NotAllowedComponent} from './components/not-allowed/not-allowed.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ResourceCardComponent} from './components/resource-card/resource-card.component';
import {Global} from './global';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {ResourceDetailComponent} from './components/resource-detail/resource-detail.component';
import {ResourceFormComponent} from './components/resource-form/resource-form.component';
import {CommentComponent} from './components/comment/comment.component';
import {ForgottenPasswordComponent} from './components/user/forgotten-password/forgotten-password.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ToastrModule} from 'ngx-toastr';
import {CommentFormComponent} from './components/comment-form/comment-form.component';
import {MatInputModule} from '@angular/material/input';
import {MatIconModule} from '@angular/material/icon';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule, MatOptionModule} from '@angular/material/core';
import {MatSelectModule} from '@angular/material/select';
import {MaterialFileInputModule} from 'ngx-material-file-input';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import {TruncatePipe} from './services/Pipe/truncate-pipe';
import {EditAccountComponent} from './components/user/edit-account/edit-account.component';
import {EditPasswordComponent} from './components/user/edit-password/edit-password.component';
import {AdminUsersComponent} from './components/admin/admin-users/admin-users.component';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatTableModule} from '@angular/material/table';
import {RolePipePipe} from './services/Pipe/role-pipe.pipe';
import {IsActivatedPipePipe} from './services/Pipe/is-activated-pipe.pipe';
import {MatDialogModule} from '@angular/material/dialog';
import {DialogAdminUserComponent} from './components/dialogs/dialog-admin-user/dialog-admin-user.component';
import {AdminResourcesComponent} from './components/admin/admin-resources/admin-resources.component';
import {DialogAdminResourceComponent} from './components/dialogs/dialog-admin-resource/dialog-admin-resource.component';
import {MatBadgeModule} from '@angular/material/badge';
import {ResourceCategoryPipePipe} from './services/Pipe/resource-category-pipe.pipe';
import {ResourceStatePipePipe} from './services/Pipe/resource-state-pipe.pipe';
import {MatSortModule} from '@angular/material/sort';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {ProfileComponent} from './layout/profile/profile.component';
import {ButtonCircleComponent} from './layout/button-circle/button-circle.component';
import {FavoritesComponent} from './components/favorites/favorites.component';
import {ResourcesSelfComponent} from './components/resources-self/resources-self.component';
import {ResourcesGalleryComponent} from './components/resources-gallery/resources-gallery.component';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {AddFriendComponent} from './components/add-friend/add-friend.component';
import {FriendsComponent} from './components/friends/friends.component';
import {UserCardComponent} from './components/user-card/user-card.component';
import { PopupShareComponent } from './components/popup-share/popup-share.component';
import { ResourcesSharedComponent } from './components/resources-shared/resources-shared.component';
import { MessageFriendsComponent } from './components/message-friends/message-friends.component';
import { ConversationComponent } from './components/conversation/conversation.component';
import { FooterComponent } from './layout/footer/footer.component';
import {MatTabsModule} from '@angular/material/tabs';
import { GeneralConditionsComponent } from './components/gdpr/general-conditions/general-conditions.component';


const routes: Routes = [
  {path: '', redirectTo: 'home', pathMatch: 'full'},
  {path: 'home', component: ResourcesComponent},
  {path: 'resources/self', component: ResourcesSelfComponent},
  {path: 'resources/favorites', component: FavoritesComponent},
  {path: 'resources/saved', component: ResourcesComponent},
  {path: 'resources/shared', component: ResourcesSharedComponent},
  {path: 'about', component: AboutComponent},
  {path: 'general_conditions', component: GeneralConditionsComponent},
  {path: 'login', component: LoginAccountComponent},
  {path: 'sign-in', component: CreateAccountComponent},
  {path: 'not-allowed', component: NotAllowedComponent},
  {path: 'resource-detail/:id', component: ResourceDetailComponent},
  {path: 'add-resource', component: ResourceFormComponent},
  {path: 'forgotten-password', component: ForgottenPasswordComponent},
  {path: 'profile/edit-account', component: EditAccountComponent},
  {path: 'profile/edit-password', component: EditPasswordComponent},
  {path: 'admin_users', component: AdminUsersComponent},
  {path: 'admin_resources', component: AdminResourcesComponent},
  {path: 'add-friends', component: AddFriendComponent},
  {path: 'friends', component: FriendsComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    NavbarComponent,
    AboutComponent,
    ResourcesComponent,
    ProfileComponent,
    CreateAccountComponent,
    LoginAccountComponent,
    NotAllowedComponent,
    ResourceCardComponent,
    ResourceDetailComponent,
    ResourceFormComponent,
    CommentComponent,
    ForgottenPasswordComponent,
    CommentFormComponent,
    TruncatePipe,
    EditAccountComponent,
    EditPasswordComponent,
    AdminUsersComponent,
    RolePipePipe,
    IsActivatedPipePipe,
    DialogAdminUserComponent,
    AdminResourcesComponent,
    DialogAdminResourceComponent,
    ResourceCategoryPipePipe,
    ResourceStatePipePipe,
    ProfileComponent,
    ButtonCircleComponent,
    FavoritesComponent,
    ResourcesSelfComponent,
    ResourcesGalleryComponent,
    AddFriendComponent,
    FriendsComponent,
    UserCardComponent,
    PopupShareComponent,
    ResourcesSharedComponent,
    MessageFriendsComponent,
    ConversationComponent,
    FooterComponent,
    GeneralConditionsComponent,
  ],
    imports: [
        BrowserModule,
        FontAwesomeModule,
        HttpClientModule,
        NgbModule,
        RouterModule.forRoot(
            routes,
            {enableTracing: false}
        ),
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        ToastrModule.forRoot({
            timeOut: 6000,
            progressBar: true
        }),
        MatInputModule,
        MatIconModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatOptionModule,
        MatSelectModule,
        MaterialFileInputModule,
        MatButtonModule,
        MatCardModule,
        MatTableModule,
        MatPaginatorModule,
        MatDialogModule,
        MatBadgeModule,
        MatSortModule,
        MatSlideToggleModule,
        MatProgressSpinnerModule,
        MatTabsModule
    ],
  providers: [
    Global,
    HttpClient
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
