FROM nginx:latest

# ^-^'
COPY ./dist/FaceGouv-Web /usr/share/nginx/html
COPY nginx.conf /etc/nginx/

EXPOSE 4200
