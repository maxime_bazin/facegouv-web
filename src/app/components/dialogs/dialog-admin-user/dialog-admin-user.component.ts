import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {UserService} from '../../../services/user.service';

@Component({
  selector: 'app-dialog-admin-user',
  templateUrl: './dialog-admin-user.component.html',
  styleUrls: ['./dialog-admin-user.component.scss']
})
export class DialogAdminUserComponent implements OnInit {

  state;

  constructor(@Inject(MAT_DIALOG_DATA) public data, private userService: UserService) {}

  ngOnInit(): void {
  }

  editUserFromAdmin(id): void {
    this.userService.editUserStateId(id, this.state);
  }

}
