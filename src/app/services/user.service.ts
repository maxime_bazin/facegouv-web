import {Global} from '../global';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {ToastrService} from 'ngx-toastr';
import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {User} from '../models/user';
import {Relation} from '../models/relation';
import {AuthService} from './auth.service';
import {SocketService} from './socket.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  //#region subjects
  usersSubject = new Subject();
  userSubject: Subject<User> = new Subject();
  relationsSubject = new Subject();
  //#endregion
  users: Array<User>;

  constructor(private global: Global, private http: HttpClient,
              private toastr: ToastrService, private authService: AuthService, private socketService: SocketService) {
  }

  async getUserById(userId): Promise<void> {
    const data = await this.http.get(`${this.global.apiUrl}user/${userId}`).toPromise();
    this.userSubject.next(data as User);
  }

  async getAllUsers(): Promise<void> {
    const data = await this.http.get(`${this.global.apiUrl}admin/all_users`).toPromise();
    this.usersSubject.next(data as Array<User>);
  }

  async getAllUsersExceptAdmin(): Promise<void> {
    const data = await this.http.get(`${this.global.apiUrl}admin/all_users_except_admin`).toPromise();
    this.usersSubject.next(data as Array<User>);
  }

  editUserStateId(id, userStateId): void {
    const stateAdmin = JSON.parse(localStorage.getItem('currentUser'))['userStateId'];
    this.http.put(`${this.global.apiUrl}admin/edit_user_state`, {id, userStateId, stateAdmin}).subscribe(() => {
      this.toastr.success('Un email a été envoyé à l\'utilisateur');
    }, (err: HttpErrorResponse) => {
      this.toastr.error(err.error.erreur);
    });
  }

  addFriend(senderId: number, receiverId: number): void {
    this.socketService.addUserToFriends(senderId, receiverId);
  }

  confirmFriend(userId: number, relationId: number): void {
    this.socketService.confirmFriendship(userId, relationId);
  }

  async deleteFriend(userId: number, relationId): Promise<void> {
    this.socketService.removeUserFromFriends(userId, relationId);
  }

  async getAllFriends(userId: number): Promise<void> {
    const data = await this.http.get(`${this.global.apiUrl}relationships/${userId}`).toPromise() as Array<Relation>;
    this.relationsSubject.next(data);
    this.authService.user.friends = data;
  }

  getFriend(relations: Relation[], userId: number, userId2: number): Relation {
    return relations?.find(f => f.SenderId === userId && f.ReceiverId === userId2 || f.SenderId === userId2 && f.ReceiverId === userId);
  }

  isFriend(relations: Relation[], userId: number): boolean {
    return relations?.some(f => f.SenderId === userId || f.ReceiverId === userId);
  }

  getOtherUserInFriend(relation: Relation, userConnectedId: number): User {
    return relation.SenderId === userConnectedId ? relation.Receiver : relation.Sender;
  }
}
