import {IEditable} from './i-editable';
import {Answer} from './answer';
import {IComment} from './i-comment';
import {User} from './user';

export class Comment implements IComment {
  constructor(editor: User) {
    this.User = editor;
  }

  content: string;
  User: User;

  updatedAt: Date;
  createdAt: Date;
  id: number;

  Sub_comments: Answer[];

}
