import {Component, Input, OnInit} from '@angular/core';
import {IComment} from '../../models/i-comment';
import {faChevronDown} from '@fortawesome/free-solid-svg-icons';
import {Comment} from '../../models/comment';
import {Global} from '../../global';
import {AppEventManagerService} from "../../services/app-event-manager.service";

@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.scss']
})
export class CommentComponent implements OnInit {
  @Input() comment: IComment;
  @Input() subCommentsDeployed;
  @Input() isComment: boolean;

  chevronRotated: boolean;
  date: Date = new Date();

  get nbAnswers(): string {
    const comment = this.comment as Comment;
    if (!comment.Sub_comments || comment.Sub_comments?.length === 0) {
      return 'répondre';
    }
    return `${comment.Sub_comments?.length} réponse${comment.Sub_comments?.length === 1 ? '' : 's'}`;
  }

  //#region icons
  faChevronDown = faChevronDown;

  //#endregion
  constructor(public global: Global) {
  }

  ngOnInit(): void {
  }

  deploySubComments(id: number): void {
    this.chevronRotated = !this.chevronRotated;
    if (this.subCommentsDeployed.includes(id)) {
      const index = this.subCommentsDeployed.indexOf(id);
      this.subCommentsDeployed.splice(index, 1);
    } else {
      this.subCommentsDeployed.push(id);
    }
  }
}
