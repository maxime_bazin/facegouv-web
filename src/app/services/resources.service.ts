import {Injectable} from '@angular/core';
import {Global} from '../global';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {ActivatedRoute, Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {Resource} from '../models/resource';
import {Observable, Subject} from 'rxjs';
import {AuthService} from './auth.service';
import {Comment} from '../models/comment';
import {Favorite} from '../models/favorite';
import {ResourceLike} from '../models/resource-like';
import {SocketService} from './socket.service';

@Injectable({
  providedIn: 'root'
})
export class ResourcesService {
  //#region subjects
  resourcesSubject = new Subject();
  resourceSubject = new Subject();
  resourcesByUserSubject = new Subject();
  numberResourcesNotValidedSubject = new Subject();
  //#endregion

  resource: Resource;

  constructor(private global: Global, private http: HttpClient, private router: Router,
              private toastr: ToastrService, private socketService: SocketService) {
  }

//region resource
  async getResourcesByNumberAndDateOrder(): Promise<void> {
    const resourcesNumber = 20; // Nombre de ressources affichés sur la homepage
    const data = await this.http.get(`${this.global.apiUrl}resources/${resourcesNumber}`).toPromise();
    this.resourcesSubject.next(data as Array<Resource>);
  }

  async getResourceById(id: number): Promise<void> {
    const data = await this.http.get(`${this.global.apiUrl}resource/${id}`).toPromise();
    this.resourceSubject.next(data as Resource);
  }

  getAllResources(): Observable<any> {
    return this.http.get(`${this.global.apiUrl}resource`);
  }

  async getAllResourcesNotValided(): Promise<any> {
    const data = await this.http.get(`${this.global.apiUrl}resource_not_valided`).toPromise();
    this.numberResourcesNotValidedSubject.next(data['data']);
  }

  async getAllResourcesByUser(userId: number): Promise<any> {
    const data = await this.http.get(`${this.global.apiUrl}resourcesByUser/${userId}`).toPromise();
    this.resourcesByUserSubject.next(data);
  }

  async getAllResourcesSharedByUserId(userId: number): Promise<any> {
    const data = await this.http.get(`${this.global.apiUrl}resources/shared/${userId}`).toPromise();
    this.resourcesSubject.next(data);
  }

  editResourceStateId(id, resourceStateId): void {
    const stateAdmin = JSON.parse(localStorage.getItem('currentUser'))['userStateId'];
    this.http.put(`${this.global.apiUrl}resource`, {id, resourceStateId, stateAdmin}).subscribe(() => {
      this.toastr.success('Ressource modifié avec succès');
    }, (err: HttpErrorResponse) => {
      this.toastr.error(err.error.erreur);
    });
  }

  async postResource(userId, resource , image): Promise<void> {
    const body = {
      userId, resource
    };
    console.log(JSON.stringify(body));
    const formData = new FormData();
    image === undefined ? image = null : formData.append('image', image, image?.name);
    formData.append('resource', JSON.stringify(body));
    await this.http.post(`${this.global.apiUrl}resource`, formData).subscribe((data) => {
      this.toastr.success('Ressource créee avec succès !');
      this.router.navigate(['']);
    }, (err: HttpErrorResponse) => {
      this.toastr.error(err.error.erreur);
    });
  }

  async likeResource(userId: number, resourceId: number): Promise<any> {
    const body = {
      userId, resourceId
    };
    await this.http.post<any>(`${this.global.apiUrl}resource/like`, body).subscribe(data => {
        this.toastr.success(data.Message);
        this.getResourceById(resourceId);
      }, error => {
        this.toastr.error(error.error.erreur);
      }
    );
  }

  async incrementView(resourceId: number): Promise<void> {
    const body = {
      resourceId
    };
    await this.http.post<any>(`${this.global.apiUrl}resource/view`, body).subscribe(data => {
      }, error => {
        console.log(error);
      }
    );
  }

  isFavorite(favorites: Array<Favorite>, resourceId: number): boolean {
    return favorites.some(f => f.Resource.id === resourceId);
  }

  isLiked(likes: Array<ResourceLike>, userId: number): boolean {
    return likes.some(f => f.UserId === userId);
  }

  shareResource(senderId: number, receiversId: number[], resourceId: number): void {
    this.socketService.shareResource(senderId, receiversId, resourceId);
  }

//endregion resource
//region comment
  async postComment(userId: number, resourceId: number, content: string): Promise<void> {
    const body = {
      userId, resourceId, content
    };
    await this.http.post(`${this.global.apiUrl}comment`, body).subscribe((data) => {
        this.toastr.success('Commentaire posté avec succès !');
      },
      (error => {
        this.toastr.error(error.error.erreur);
      }));
  }

//endregion comment
//region subComment
  async postSubComment(userId: number, commentId: number, content: string): Promise<void> {
    const body = {
      userId, commentId, content
    };
    await this.http.post(`${this.global.apiUrl}subComment`, body).subscribe((data) => {
        this.toastr.success('Réponse ajouté avec succès !');
      },
      (error => {
        this.toastr.error(error.error.erreur);
      })
    );
  }

//endregion subComment
}
