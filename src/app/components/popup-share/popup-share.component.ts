import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';
import {Relation} from '../../models/relation';
import {UserService} from '../../services/user.service';
import {AuthService} from '../../services/auth.service';
import {User} from '../../models/user';
import {Resource} from '../../models/resource';
import {ToastrService} from 'ngx-toastr';
import {ResourcesService} from '../../services/resources.service';
import {AppEventManagerService} from '../../services/app-event-manager.service';
import {Global} from "../../global";

@Component({
  selector: 'app-popup-share',
  templateUrl: './popup-share.component.html',
  styleUrls: ['./popup-share.component.scss']
})
export class PopupShareComponent implements OnInit {
  friends: Relation[];
  @Input() resource: Resource;
  @Input() isOpen: boolean;
  @Output() isOpenChange = new EventEmitter<boolean>();
  usersSelected: number[] = new Array<number>();

  constructor(private userService: UserService, private authService: AuthService,
              private toastr: ToastrService, private resourceService: ResourcesService,
              private appEventManagerService: AppEventManagerService, public global: Global) {
  }

  async ngOnInit(): Promise<void> {
    this.subscribeToFriends();
    await this.userService.getAllFriends(this.authService.user.id);
    this.appEventManagerService.subscribe('resourceShared', () => this.closePopup());
  }

  subscribeToFriends(): void {
    this.userService.relationsSubject.subscribe(
      (friends: Relation[]) => {
        this.friends = friends.filter(value => value.isConfirmed);
      },
      error => {
        console.error(error);
      }
    );
  }

  getOtherUserInFriend(friend: Relation): User {
    return this.userService.getOtherUserInFriend(friend, this.authService.user.id);
  }

  closePopup(): void {
    this.isOpen = false;
    this.isOpenChange.emit(this.isOpen);
  }

  addUser(user: User): void {
    if (this.isSelected(user)) {
      this.usersSelected = this.usersSelected.filter(id => id !== user.id);
    } else {
      this.usersSelected.push(user.id);
    }
  }

  isSelected(user: User): boolean {
    return this.usersSelected.some(id => id === user.id);
  }

  share(): void {
    if (this.usersSelected.length === 0) {
      this.toastr.error('Vous n\'avez sélectionné aucun ami');
    } else {
      this.resourceService.shareResource(this.authService.user.id, this.usersSelected, this.resource.id);
    }
  }
}
