import {Injectable} from '@angular/core';
import {User} from '../models/user';
import {Subject} from 'rxjs';
import {Global} from '../global';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {ActivatedRoute, Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {SocketService} from './socket.service';
import {Favorite} from '../models/favorite';
import {FavoriteService} from './favorite.service';
import { AppEventManagerService } from './app-event-manager.service';


const EMAIL_REGEX = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/;
const PASSWORD_REGEX = /^(?=.*[0-9]+.*)(?=.*[a-zA-Z]+.*)[0-9a-zA-Z]{6,}$/;

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  userSubject = new Subject();
  user: User;

  constructor(private appEvent: AppEventManagerService, private global: Global, private http: HttpClient, private router: Router,
              private toastr: ToastrService, private route: ActivatedRoute,
              private socketService: SocketService) {
    this.initLocalStorageUser();
  }

  signIn(user: User, confirmPwd): void {

    if (!EMAIL_REGEX.test(user.email)) {
      this.toastr.error('Ceci n\'est pas une adresse email');
      return;
    }

    if (!PASSWORD_REGEX.test(user.password)) {
      this.toastr.error('Le mot de passe doit contenir au moins 6 caractères, 1 lettre et 1 chiffre');
      return;
    }

    if (user.password === confirmPwd) {
      this.http.post(`${this.global.apiUrl}users/register/`, user).subscribe(() => {
        this.toastr.success('Inscription réussie, vous avez reçu un email afin d\'activer votre compte');
        this.router.navigate(['/login'], {relativeTo: this.route});
      }, (err: HttpErrorResponse) => {
        this.toastr.error(err.error.erreur);
      });
    } else {
      this.toastr.error('Le mot de passe n\'est pas le même');
    }
  }

  async login(email, password): Promise<void> {
    try {
      const auth = {email, password};
      const token = await this.http.post(`${this.global.apiUrl}users/login`, auth).toPromise();
      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`
        })
      };
      const data = await this.http.get(`${this.global.apiUrl}users/protected`, httpOptions).toPromise();
      this.user = data['user'] as User;
      localStorage.setItem('currentUser', JSON.stringify(this.user));
      await this.router.navigate(['']);
      this.userSubject.next(this.user);
      this.toastr.success(`Bonjour ${this.user.firstName}`);
      this.socketService.turnOnSocket(this.user.id);
      this.appEvent.broadcast('userConnected');
    } catch (err) {
      this.toastr.error(err.error.erreur);
    }
  }

  forgottenPassword(email): void {
    this.http.post(`${this.global.apiUrl}users/forgotten_password`, {email}).subscribe(() => {
      this.toastr.success('Un email vous a été envoyé');
      this.router.navigate(['/login']);
    });
  }

  async editAccount(user: User, image): Promise<void> {
    const formData = new FormData();
    image === undefined ? image = null : formData.append('image', image, image?.name);
    formData.append('user', JSON.stringify(user));
    await this.http.put(`${this.global.apiUrl}users/edit_account`, formData).subscribe((res) => {
      localStorage.setItem('currentUser', JSON.stringify(res['user']));
      this.toastr.success('Profil mis à jour avec succès');
    }, (err: HttpErrorResponse) => {
      this.toastr.error(err.error.erreur);
    });
  }

  editPassword(password, newPassword, confirmPassword): void {
    if (!PASSWORD_REGEX.test(newPassword)) {
      this.toastr.error('Le mot de passe doit contenir au moins 6 caractères, 1 lettre et 1 chiffre');
      return;
    }

    if (newPassword === confirmPassword) {
      this.http.put(`${this.global.apiUrl}users/edit_password`, {
        id: this.user.id,
        password,
        newPassword
      }).subscribe(() => {
        this.toastr.success('Mot de passe mis à jour avec succès');
        this.router.navigate(['/profile']);
      }, (err: HttpErrorResponse) => {
        this.toastr.error(err.error.erreur);
      });
    } else {
      this.toastr.error('Le mot de passe n\'est pas le même');
      return;
    }
  }

  async logout(): Promise<void> {
    this.toastr.success(`À bientôt ${this.user.firstName}`);
    localStorage.removeItem('currentUser');
    this.user = null;
    this.userSubject.next(null);
    this.appEvent.broadcast('userDisconnected');
    await this.socketService.turnOffSocket();
    await this.router.navigate(['login']);
  }

  initLocalStorageUser(): void {
    const userLocal = localStorage.getItem('currentUser');
    if (userLocal && userLocal !== 'undefined') {
      this.user = JSON.parse(userLocal) as User;
      this.userSubject.next(this.user);
      this.socketService.turnOnSocket(this.user.id);
    }
  }

  async getUserConnected(): Promise<void> {
    this.userSubject.next(this.user);
  }

  isConnected(): boolean {
    return this.user != null;
  }
}
